import { useState, useEffect } from 'react'
import { useNavigate, Link } from 'react-router-dom'
import { useSignupMutation } from '../app/apiSlice'

export default function SignUpForm() {
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        username: '',
        password: '',
        full_name: '',
        email: '',
        linkedin_url: '',
    })
    const [errorMessage, setErrorMessage] = useState('')
    const [showModal, setShowModal] = useState(false)
    const [signup, signupStatus] = useSignupMutation()

    useEffect(() => {
        if (signupStatus.isSuccess) {
            navigate('/')
        } else if (signupStatus.isError) {
            setErrorMessage(signupStatus.error.data.detail)
            setShowModal(true)
        }
    }, [signupStatus, navigate, setErrorMessage, setShowModal])

    const handleFormSubmit = (e) => {
        e.preventDefault()
        if (
            !formData.username ||
            !formData.password ||
            !formData.email ||
            !formData.linkedin_url ||
            !formData.full_name
        ) {
            setErrorMessage('Please fill out all required fields.')
            setShowModal(true)
        } else {
            signup(formData)
        }
    }

    const closeModal = () => {
        setShowModal(false)
        setErrorMessage('')
    }

    return (
        <>
            {showModal && (
                <div
                    className="modal"
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        position: 'fixed',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        zIndex: 9999,
                    }}
                >
                    <div
                        className="modal-dialog"
                        role="document"
                        style={{ width: '30%', textAlign: 'center' }}
                    >
                        <div className="modal-content">
                            <div
                                className="modal-header"
                                style={{
                                    backgroundColor: '#dda3a6',
                                    textAlign: 'center',
                                    position: 'relative',
                                    height: '60px',
                                }}
                            >
                                <button
                                    type="button"
                                    className="btn-close"
                                    onClick={closeModal}
                                    style={{
                                        position: 'absolute',
                                        top: '10px',
                                        right: '10px',
                                        color: '#fff',
                                        fontSize: '0.8rem',
                                    }}
                                    aria-label="Close"
                                ></button>
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="35"
                                        height="35"
                                        fill="currentColor"
                                        className="bi bi-exclamation-triangle"
                                        viewBox="0 0 16 16"
                                        style={{ marginRight: '5px' }}
                                    >
                                        <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.15.15 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.2.2 0 0 1-.054.06.1.1 0 0 1-.066.017H1.146a.1.1 0 0 1-.066-.017.2.2 0 0 1-.054-.06.18.18 0 0 1 .002-.183L7.884 2.073a.15.15 0 0 1 .054-.057m1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767z" />
                                        <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z" />
                                    </svg>
                                    <h3
                                        style={{
                                            margin: '0',
                                            color: 'black',
                                        }}
                                    >
                                        Uh-oh!
                                    </h3>
                                </div>
                            </div>
                            <div
                                className="modal-body"
                                style={{
                                    paddingTop: '12px',
                                    paddingBottom: '0px',
                                }}
                            >
                                <p>{errorMessage}</p>
                            </div>
                            <div
                                className="modal-footer"
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: 'white',
                                    borderTop: 'none',
                                    paddingBottom: '10px',
                                    paddingTop: '0px',
                                }}
                            >
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    onClick={closeModal}
                                    style={{ borderRadius: '0' }}
                                >
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )}

            <div
                className="d-flex justify-content-center align-items-center"
                style={{
                    minHeight: '80vh',
                }}
            >
                <form
                    onSubmit={handleFormSubmit}
                    style={{
                        width: '50%',
                        boxShadow: '0px 4px 30px rgba(225, 212, 229, 0.8)',
                        padding: '20px',
                    }}
                >
                    <h3 className="form-header text-center">Create Account</h3>
                    <div
                        className="form-group row"
                        style={{
                            paddingBottom: '10px',
                            paddingTop: '20px',
                        }}
                    >
                        <label
                            htmlFor="username"
                            className="col-sm-2 col-form-label"
                        >
                            Username*
                        </label>
                        <div className="col-sm-10">
                            <input
                                type="text"
                                id="username"
                                className="form-control"
                                value={formData.username}
                                onChange={(e) =>
                                    setFormData({
                                        ...formData,
                                        username: e.target.value,
                                    })
                                }
                                placeholder="Username"
                                required
                            />
                        </div>
                    </div>
                    <div
                        className="form-group row"
                        style={{
                            paddingBottom: '10px',
                            paddingTop: '10px',
                        }}
                    >
                        <label
                            htmlFor="password"
                            className="col-sm-2 col-form-label"
                        >
                            Password*
                        </label>
                        <div className="col-sm-10">
                            <input
                                type="password"
                                id="password"
                                className="form-control"
                                value={formData.password}
                                onChange={(e) =>
                                    setFormData({
                                        ...formData,
                                        password: e.target.value,
                                    })
                                }
                                placeholder="Password"
                                required
                            />
                        </div>
                    </div>
                    <div
                        className="form-group row"
                        style={{
                            paddingBottom: '10px',
                            paddingTop: '10px',
                        }}
                    >
                        <label
                            htmlFor="full_name"
                            className="col-sm-2 col-form-label"
                        >
                            Full Name*
                        </label>
                        <div className="col-sm-10">
                            <input
                                type="text"
                                id="full_name"
                                className="form-control"
                                value={formData.full_name}
                                onChange={(e) =>
                                    setFormData({
                                        ...formData,
                                        full_name: e.target.value,
                                    })
                                }
                                placeholder="Full Name"
                            />
                        </div>
                    </div>
                    <div
                        className="form-group row"
                        style={{
                            paddingBottom: '10px',
                            paddingTop: '10px',
                        }}
                    >
                        <label
                            htmlFor="email"
                            className="col-sm-2 col-form-label"
                        >
                            Email*
                        </label>

                        <div className="col-sm-10">
                            <input
                                type="email"
                                id="email"
                                className="form-control"
                                value={formData.email}
                                onChange={(e) =>
                                    setFormData({
                                        ...formData,
                                        email: e.target.value,
                                    })
                                }
                                placeholder="Email"
                                required
                            />
                        </div>
                    </div>
                    <div
                        className="form-group row"
                        style={{
                            paddingBottom: '20px',
                            paddingTop: '10px',
                        }}
                    >
                        <label
                            htmlFor="linkedin_url"
                            className="col-sm-2 col-form-label"
                        >
                            LinkedIn URL*
                        </label>
                        <div className="col-sm-10">
                            <input
                                type="text"
                                id="linkedin_url"
                                className="form-control"
                                value={formData.linkedin_url}
                                onChange={(e) =>
                                    setFormData({
                                        ...formData,
                                        linkedin_url: e.target.value,
                                    })
                                }
                                placeholder="LinkedIn URL"
                                required
                            />
                        </div>
                    </div>

                    <div
                        className="form-footer"
                        style={{
                            paddingBottom: '5px',
                            paddingTop: '20px',
                            display: 'flex',
                            justifyContent: 'center',
                        }}
                    >
                        <button
                            type="submit"
                            className="btn"
                            style={{
                                backgroundColor: '#F2CEAE',
                                color: 'white',
                                width: '15%',
                                borderRadius: '20px',
                            }}
                            onClick={handleFormSubmit}
                            required
                        >
                            Sign Up
                        </button>
                    </div>
                    <p
                        style={{
                            textAlign: 'center',
                            paddingTop: '10px',
                        }}
                    >
                        Have an account?&nbsp;
                        <Link to="/signin">Sign In</Link>
                    </p>
                </form>
            </div>
        </>
    )
}
