import { Link } from 'react-router-dom'
import React from 'react'
import { useState, useEffect } from 'react'
import { useAuthenticateQuery } from '../app/apiSlice'

function about() {
    const { data: user, isLoading: isLoadingUser } = useAuthenticateQuery()

    const [openAccordionItem, setOpenAccordionItem] = useState(null)

    const toggleAccordionItem = (index) => {
        setOpenAccordionItem(openAccordionItem === index ? null : index)
    }

    if (isLoadingUser) {
        return <div>Loading...</div>
    }

    const accordionItems = [
        {
            title: 'How does GitGirl work?',
            body: 'Employers post job openings on our platform, and job seekers can apply for positions that match their skills and interests.',
        },
        {
            title: 'How does GitGirl ensure job postings are legitimate?',
            body: 'We thoroughly vet all job postings and employers to ensure they meet our standards of inclusivity and professionalism.',
        },
        {
            title: 'Are there any community features on GitGirl?',
            body: 'It is our goal to offer networking events in the future. In the meantime, you can reach us at gitgirl.co@gmail.com',
        },
        {
            title: 'Is there a cost to use GitGirl?',
            body: 'Job seekers can use GitGirl for free to search and apply for jobs. Employers can choose from various posting plans, depending on their needs.',
        },
    ]

    return (
        <div>
            <div className="container">
                <div
                    className="row justify-content-center"
                    style={{
                        marginTop: '150px',
                        marginBottom: '40px',
                    }}
                >
                    <div className="col-md-6">
                        <div className="grid-container">
                            <div className="grid-box">
                                <div className="d-flex align-items-center">
                                    <div
                                        className="vl"
                                        style={{
                                            paddingLeft: '3px',
                                            borderLeft: '3px dotted #F2CEAE',
                                            height: '45px',
                                        }}
                                    >
                                        <h1
                                            className="mb-2"
                                            style={{
                                                marginLeft: '10px',
                                                color: '#5a414b',
                                            }}
                                        >
                                            About Us
                                        </h1>
                                    </div>
                                </div>
                                <div>
                                    <p
                                        style={{
                                            marginTop: '20px',
                                        }}
                                    >
                                        GitGirl is a dynamic community committed
                                        to empowering women and
                                        femme-identifying individuals in the
                                        tech industry. Our platform is designed
                                        to bridge the gap between talented
                                        professionals and leading companies,
                                        offering access to exciting
                                        opportunities and fostering meaningful
                                        connections. We believe that diversity
                                        is the key to innovation and
                                        productivity, and we strive to create an
                                        environment where everyone's unique
                                        skills and perspectives are valued and
                                        celebrated.
                                        <br />
                                        <br />
                                        Founded with a vision to break down
                                        barriers and challenge the status quo,
                                        GitGirl is dedicated to building a more
                                        inclusive and equitable tech landscape.
                                        We offer resources, support, and a
                                        network of like-minded individuals who
                                        are passionate about driving change.
                                        Whether you're looking to advance your
                                        career, gain new skills, or connect with
                                        industry leaders, GitGirl is here to
                                        help you thrive. Join us on our mission
                                        to transform the tech industry and pave
                                        the way for a brighter, more inclusive
                                        future.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        className="col-md-5"
                        style={{ paddingLeft: '40px', paddingTop: '5px' }}
                    >
                        <div className="grid-container">
                            <div className="grid-box">
                                <h3 className="text-center">FAQ</h3>
                                <div
                                    className="accordion"
                                    id="accordionFAQ"
                                    style={{
                                        width: '90%',
                                        margin: 'auto',
                                        paddingTop: '20px',
                                        paddingBottom: '50px',
                                    }}
                                >
                                    {accordionItems.map((item, index) => (
                                        <div
                                            className="accordion-item"
                                            key={index}
                                        >
                                            <h2
                                                className="accordion-header"
                                                id={`heading${index}`}
                                            >
                                                <button
                                                    className={`accordion-button ${
                                                        openAccordionItem ===
                                                        index
                                                            ? ''
                                                            : 'collapsed'
                                                    }`}
                                                    type="button"
                                                    onClick={() =>
                                                        toggleAccordionItem(
                                                            index
                                                        )
                                                    }
                                                    style={{
                                                        fontWeight: 'bold',
                                                        fontFamily: 'Ariel',
                                                    }}
                                                >
                                                    {item.title}
                                                </button>
                                            </h2>
                                            <div
                                                id={`collapse${index}`}
                                                className={`accordion-collapse collapse ${
                                                    openAccordionItem === index
                                                        ? 'show'
                                                        : ''
                                                }`}
                                                aria-labelledby={`heading${index}`}
                                                data-bs-parent="#accordionExample"
                                            >
                                                <div className="accordion-body">
                                                    {item.body}
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-5">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '220px',
                                    marginBottom: '40px',
                                }}
                            >
                                <img
                                    className="d-block user-select-none img-fluid"
                                    src="/src/GitGirl-about.jpeg"
                                    alt="Sample"
                                    style={{
                                        borderRadius: '10px',
                                        height: '100%',
                                        width: '90%',
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '145px',
                                    marginBottom: '140px',
                                }}
                            >
                                <div className="d-flex align-items-center">
                                    <div
                                        className="vl"
                                        style={{
                                            paddingLeft: '3px',
                                            borderLeft: '3px dotted #F2CEAE',
                                            height: '45px',
                                        }}
                                    >
                                        <h1
                                            className="mb-2"
                                            style={{
                                                marginLeft: '10px',
                                                color: '#5a414b',
                                            }}
                                        >
                                            Our Mission
                                        </h1>
                                    </div>
                                </div>
                                <div>
                                    <p
                                        style={{
                                            marginTop: '20px',
                                        }}
                                    >
                                        At GitGirl, our mission is to empower
                                        women and femme-identifying individuals
                                        in the tech industry by providing access
                                        to opportunities and fostering an
                                        inclusive community. We are dedicated to
                                        promoting diversity and breaking down
                                        barriers to create a tech landscape
                                        where everyone can excel.
                                        <br />
                                        <br />
                                        Our commitment is to cultivate a
                                        platform that celebrates talent from all
                                        backgrounds, driving innovation and
                                        addressing the challenges of the digital
                                        world with diverse perspectives. We
                                        believe that through collaboration and
                                        inclusivity, we can build a more
                                        equitable and dynamic tech industry.
                                        <br />
                                        <br />
                                        Join us at GitGirl in our mission to
                                        champion diversity, shatter stereotypes,
                                        and create pathways for all individuals
                                        to succeed. Together, we will shape a
                                        brighter, more inclusive future in tech.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!user && (
                <div
                    className="flex-row"
                    style={{
                        display: 'flex',
                        marginLeft: '10%',
                        marginRight: '10%',
                        paddingBottom: '140px',
                    }}
                >
                    <div style={{ paddingRight: '5px', width: '50%' }}>
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Become A Partner</h5>
                                <div
                                    className="vl"
                                    style={{
                                        paddingLeft: '10px',
                                        borderLeft: '3px dotted #F2CEAE',
                                        height: '30%',
                                    }}
                                >
                                    <p className="card-text">
                                        Find your next hire.
                                    </p>
                                </div>
                                <div className="d-flex justify-content-center mt-3">
                                    <div
                                        className="col"
                                        style={{ marginLeft: '80%' }}
                                    >
                                        <Link
                                            to="/signup"
                                            type="button"
                                            className="btn btn-block"
                                            style={{
                                                backgroundColor: 'white',
                                                color: 'black',
                                                border: '1px solid black',
                                                borderRadius: '20px',
                                                textAlign: 'center',
                                                width: '130px',
                                                marginRight: '60px',
                                                marginTop: '20px',
                                            }}
                                        >
                                            Join Network
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style={{ paddingRight: '5px', width: '50%' }}>
                        <div className="card">
                            <div
                                className="card-body"
                                style={{
                                    backgroundColor: '#B695BF',
                                    color: 'white',
                                }}
                            >
                                <h5 className="card-title">Become A Partner</h5>
                                <div
                                    className="vl"
                                    style={{
                                        paddingLeft: '10px',
                                        borderLeft: '3px dotted #F2CEAE',
                                        height: '30%',
                                    }}
                                >
                                    <p className="card-text">
                                        Find your next hire.
                                    </p>
                                </div>
                                <div className="d-flex justify-content-center mt-3">
                                    <div
                                        className="col"
                                        style={{ marginLeft: '80%' }}
                                    >
                                        <Link
                                            to="/signup"
                                            type="button"
                                            className="btn btn-block"
                                            style={{
                                                backgroundColor: '#B695BF',
                                                color: 'white',
                                                border: '1px solid white',
                                                borderRadius: '20px',
                                                textAlign: 'center',
                                                width: '130px',
                                                marginRight: '60px',
                                                marginTop: '20px',
                                            }}
                                        >
                                            Join Network
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}

export default about
