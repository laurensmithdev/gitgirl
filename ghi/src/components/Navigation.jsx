import { useEffect } from 'react'
import { NavLink, useNavigate, Link } from 'react-router-dom'
import { useAuthenticateQuery, useSignoutMutation } from '../app/apiSlice'

const Navigation = () => {
    const navigate = useNavigate()
    const { data: user, isLoading: isLoadingUser } = useAuthenticateQuery()
    const [signout, signoutStatus] = useSignoutMutation()

    useEffect(() => {
        if (signoutStatus.isSuccess) navigate('/')
    }, [signoutStatus])

    const onSignoutClick = () => {
        signout()
    }

    return (
        <nav
            className="navbar navbar-expand-lg"
            style={{
                backgroundColor: 'white',
                height: '90px',
                marginBottom: '50px',
            }}
            data-bs-theme="light"
        >
            <div className="container-fluid">
                <Link to="/">
                    <img
                        src="/src/GG-Logo-Only.png"
                        height="70px"
                        style={{
                            paddingLeft: '20px',
                            paddingRight: '20px',
                        }}
                        alt="Logo"
                    />
                </Link>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarColor01"
                    aria-controls="navbarColor01"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarColor01">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink
                                className="nav-link"
                                to="/about"
                                style={{ fontFamily: 'Arial' }}
                            >
                                About Us
                            </NavLink>
                        </li>
                        {user && (
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    to="/jobs"
                                    style={{ fontFamily: 'Arial' }}
                                >
                                    Jobs
                                </NavLink>
                            </li>
                        )}
                    </ul>

                    <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                        {!isLoadingUser && user ? (
                            <>
                                <li className="nav-item dropdown">
                                    <span
                                        className="nav-link dropdown-toggle"
                                        id="navbarDropdown"
                                        role="button"
                                        style={{
                                            color: 'grey',
                                            fontFamily: 'Arial',
                                            cursor: 'pointer',
                                        }}
                                    >
                                        Welcome back, {user.username}!
                                    </span>
                                    <ul
                                        className="dropdown-menu"
                                        aria-labelledby="navbarDropdown"
                                    >
                                        <li>
                                            <NavLink
                                                className="dropdown-item"
                                                to="/profile"
                                            >
                                                Profile
                                            </NavLink>
                                        </li>
                                    </ul>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        type="button"
                                        className="btn btn-block"
                                        onClick={onSignoutClick}
                                        style={{
                                            backgroundColor: '#F2CEAE',
                                            color: 'white',
                                            border: '0',
                                            borderRadius: '20px',
                                            textAlign: 'center',
                                            width: '100px',
                                            marginRight: '20px',
                                            marginLeft: '5px',
                                            marginTop: '2px',
                                        }}
                                    >
                                        Sign Out
                                    </Link>
                                </li>
                            </>
                        ) : (
                            <>
                                <li className="nav-item">
                                    <Link
                                        to="/signin"
                                        type="button"
                                        className="btn btn-block"
                                        style={{
                                            backgroundColor: 'white',
                                            color: 'black',
                                            border: '1px solid black',
                                            borderRadius: '20px',
                                            textAlign: 'center',
                                            width: '100px',
                                            marginRight: '20px',
                                            marginTop: '0px',
                                        }}
                                    >
                                        Sign In
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        to="/signup"
                                        type="button"
                                        className="btn btn-block"
                                        style={{
                                            backgroundColor: '#F2CEAE',
                                            color: 'white',
                                            borderRadius: '20px',
                                            textAlign: 'center',
                                            width: '130px',
                                            marginRight: '20px',
                                            marginTop: '0px',
                                        }}
                                    >
                                        Join GitGirl
                                    </Link>
                                </li>
                            </>
                        )}
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navigation
