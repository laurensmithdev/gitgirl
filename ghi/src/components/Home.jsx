import { useState } from 'react'
import { Link } from 'react-router-dom'
import { useAuthenticateQuery } from '../app/apiSlice'
import MovingComponent from 'react-moving-text'
// import banner_3 from '/src/banner_3.jpg'

function Home() {
    const { data: user, isLoading: isLoadingUser } = useAuthenticateQuery()
    const [activeIndex, setActiveIndex] = useState(0)

    const handlePrev = () => {
        setActiveIndex(activeIndex === 0 ? reviews.length - 1 : activeIndex - 1)
    }

    const handleNext = () => {
        setActiveIndex(activeIndex === reviews.length - 1 ? 0 : activeIndex + 1)
    }

    const reviews = [
        {
            name: 'Sierra R',
            position: 'Director of Engineering - Apple',
            image: '/src/sr.png',
            testimonial:
                "The hires I've made from GitGirl have been top notch! We will continue to hire from this platform as we stay dedicated to increasing the number of Women in Tech!",
        },
        {
            name: 'Jay W.',
            position: 'CEO & Founder - Too many companies to list',
            image: '/src/jw.png',
            testimonial: 'No one can hold a candle to the GitGirl devs!',
        },
        {
            name: 'Chris Z.',
            position: 'VP of Product - Meta',
            image: '/src/chris.png',
            testimonial:
                "Every hire we've made has exceeded expectations. Simply put, GitGirl is a game-changer in the tech hiring landscape.",
        },
        {
            name: 'Brittany R.',
            position: 'Director of Eng - Amazon',
            image: '/src/bb.png',
            testimonial:
                'GitGirl has been an incredible platform for me to find opportunities in tech. The supportive community and the focus on diversity make it stand out.',
        },
    ]

    if (isLoadingUser) {
        return <div>Loading...</div>
    }

    return (
        <div style={{ backgroundColor: '#FCFCFD' }}>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '160px',
                                    marginBottom: '30px',
                                    width: '100%',
                                }}
                            >
                                <h1
                                    style={{
                                        marginBottom: '10px',
                                        fontFamily: 'Trirong',
                                        fontSize: '90px',
                                        color: '#404040',
                                    }}
                                >
                                    Welcome to
                                </h1>

                                <div className="typewriter">
                                    <h1
                                        style={{
                                            marginBottom: '30px',
                                            fontFamily: 'Trirong',
                                            fontSize: '90px',
                                            color: '#404040',
                                        }}
                                    >
                                        GitGirl
                                    </h1>
                                </div>

                                <div
                                    className="vl"
                                    style={{
                                        paddingLeft: '10px',
                                        borderLeft: '3px dotted #F2CEAE',
                                        height: '80px',
                                    }}
                                >
                                    <p
                                        className="mb-9"
                                        style={{
                                            marginLeft: '10px',
                                            display: 'inline-block',
                                            width: '85%',
                                            color: 'black',
                                            fontFamily: 'Arial',
                                        }}
                                    >
                                        Ready to join a community focused on
                                        helping women grow their careers in
                                        tech? We support women in all areas of
                                        tech, including engineering, design,
                                        product management, and more.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div
                            className="d-flex justify-content-center mt-3"
                            style={{ width: '75%', gap: '30px' }}
                        >
                            <div>
                                {user ? (
                                    <Link
                                        to="/profile"
                                        type="button"
                                        className="btn btn-block"
                                        style={{
                                            backgroundColor: '#B695BF',
                                            color: 'white',
                                            border: 'none',
                                            borderRadius: '20px',
                                            textAlign: 'center',
                                            width: '100%',

                                            marginTop: '10px',
                                        }}
                                    >
                                        Profile
                                    </Link>
                                ) : (
                                    <Link
                                        to="/signup"
                                        type="button"
                                        className="btn btn-block"
                                        style={{
                                            backgroundColor: '#B695BF',
                                            color: 'white',
                                            border: 'none',
                                            borderRadius: '20px',
                                            textAlign: 'center',
                                            width: '110%',
                                            marginTop: '10px',
                                        }}
                                    >
                                        Join Network
                                    </Link>
                                )}
                            </div>
                            <div>
                                {user ? (
                                    <Link
                                        to="/applications"
                                        type="button"
                                        className="btn btn-block"
                                        style={{
                                            backgroundColor: 'white',
                                            color: 'black',
                                            border: '1px solid black',
                                            borderRadius: '20px',
                                            textAlign: 'center',
                                            width: '110%',
                                            marginTop: '10px',
                                        }}
                                    >
                                        My Applications
                                    </Link>
                                ) : (
                                    <Link
                                        to="/signin"
                                        type="button"
                                        className="btn btn-block"
                                        style={{
                                            backgroundColor: 'white',
                                            color: 'black',
                                            border: '1px solid black',
                                            borderRadius: '20px',
                                            textAlign: 'center',
                                            width: '110%',
                                            marginTop: '10px',
                                        }}
                                    >
                                        Sign In
                                    </Link>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-5">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginBottom: '140px',
                                    marginTop: '60px',
                                }}
                            >
                                <img
                                    className="d-block user-select-none img-fluid"
                                    src="/src/home-welcome.jpeg"
                                    alt="Sample"
                                    style={{
                                        borderRadius: '200px',
                                        marginLeft: '30px',
                                        marginTop: '200px',
                                        boxShadow:
                                            '0px 5px 30px rgba(126, 119, 166, 0.3)',
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    margin: '20px 0',
                    marginTop: '100px',
                }}
            >
                <div
                    style={{
                        flex: 1,
                        paddingRight: '3px',
                        borderTop: '3px dotted #F2CEAE',
                        maxWidth: '100px',
                    }}
                ></div>
                <h1
                    style={{
                        margin: '0 10px',
                        fontFamily: 'Trirong',
                        color: '#362C38',
                    }}
                >
                    What We Offer
                </h1>
                <div
                    style={{
                        flex: 1,
                        paddingLeft: '3px',
                        borderTop: '3px dotted #F2CEAE',
                        maxWidth: '100px',
                    }}
                ></div>
            </div>
            <div className="container d-flex justify-content-center">
                <div className="row justify-content-center">
                    <div className="col-3 m-4">
                        <div className="d-flex justify-content-center">
                            <div
                                className="p-3 mb-5 bg-body-tertiary rounded"
                                style={{
                                    height: '350px',
                                    width: '600px',
                                    textAlign: 'center',
                                    borderRadius: '40px',
                                    boxShadow:
                                        '0px 0px 50px rgba(210, 208, 216, 0.9)',
                                }}
                            >
                                <img
                                    src="/src/home_2.png"
                                    alt="Working Woman"
                                    style={{
                                        height: '120px',
                                        width: '120px',
                                        objectFit: 'contain',
                                        display: 'block',
                                        margin: 'auto',
                                    }}
                                />
                                <p>{''}</p>
                                <h5
                                    style={{
                                        fontFamily: 'Ariel bold',
                                        fontWeight: '600',
                                    }}
                                >
                                    Increased Visibility
                                </h5>
                                <p>{''}</p>
                                <p
                                    style={{
                                        fontFamily: 'Ariel ',
                                    }}
                                >
                                    Creating a profile on GitGirl boosts your
                                    visibility to inclusive companies actively
                                    seeking women in tech.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-3 m-4">
                        <div className="d-flex justify-content-center">
                            <div
                                className="p-3 mb-5 bg-body-tertiary rounded"
                                style={{
                                    height: '350px',
                                    width: '600px',
                                    textAlign: 'center',
                                    borderRadius: '40px',
                                    boxShadow:
                                        '0px 0px 50px rgba(210, 208, 216, 0.9)',
                                }}
                            >
                                <img
                                    src="/src/home_1.png"
                                    alt="Working Woman"
                                    style={{
                                        height: '120px',
                                        width: '120px',
                                        objectFit: 'contain',
                                        display: 'block',
                                        margin: 'auto',
                                    }}
                                />
                                <p>{''}</p>
                                <h5
                                    style={{
                                        fontFamily: 'Ariel bold',
                                        fontWeight: '600',
                                    }}
                                >
                                    Addressing the Gender Gap
                                </h5>
                                <p>{''}</p>
                                <p
                                    style={{
                                        fontFamily: 'Ariel ',
                                    }}
                                >
                                    GitGirl is committed to bridging the gender
                                    gap. As of 2024, women make up about 33% of
                                    the tech space.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-3 m-4">
                        <div className="d-flex justify-content-center">
                            <div
                                className="p-3 mb-5 bg-body-tertiary rounded"
                                style={{
                                    height: '350px',
                                    width: '600px',
                                    textAlign: 'center',
                                    borderRadius: '40px',
                                    boxShadow:
                                        '0px 0px 50px rgba(210, 208, 216, 0.9)',
                                }}
                            >
                                <img
                                    src="/src/home_3.png"
                                    alt="Working Woman"
                                    style={{
                                        height: '120px',
                                        width: '120px',
                                        objectFit: 'contain',
                                        display: 'block',
                                        margin: 'auto',
                                    }}
                                />
                                <p>{''}</p>
                                <h5
                                    style={{
                                        fontFamily: 'Ariel bold',
                                        fontWeight: '600',
                                    }}
                                >
                                    Untapped Potential
                                </h5>
                                <p>{''}</p>
                                <p
                                    style={{
                                        fontFamily: 'Ariel ',
                                    }}
                                >
                                    GitGirl connects employers with qualified
                                    women in tech often overlooked on
                                    traditional job boards.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div
                className="container"
                style={{
                    backgroundColor: 'white',
                    boxShadow: '0px 0px 50px rgba(210, 208, 216, 0.6)',
                }}
            >
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '150px',
                                    marginBottom: '40px',
                                }}
                            >
                                <img
                                    className="d-block user-select-none img-fluid"
                                    src="/src/gitgirl-women.jpeg"
                                    alt="Sample"
                                    style={{
                                        borderRadius: '20px',
                                        height: '100%',
                                        width: '90%',
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-5">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '170px',
                                    marginBottom: '40px',
                                }}
                            >
                                <div className="d-flex align-items-center">
                                    <div
                                        className="vl"
                                        style={{
                                            paddingLeft: '3px',
                                            borderLeft: '3px dotted #F2CEAE',
                                            height: '45px',
                                        }}
                                    >
                                        <h1
                                            className="mb-2"
                                            style={{
                                                marginLeft: '10px',
                                                color: '#5a414b',
                                                fontFamily: 'Trirong',
                                                fontWeight: '700',
                                            }}
                                        >
                                            For Individuals
                                        </h1>
                                    </div>
                                </div>
                                <div>
                                    <p
                                        style={{
                                            marginTop: '10px',
                                            fontFamily: 'Ariel',
                                        }}
                                    >
                                        Are you a tech enthusiast eager to
                                        advance your career? GitGirl welcomes
                                        women and femme-identifying individuals
                                        to join our platform and unlock a world
                                        of opportunities. Access a wide range of
                                        job listings from top employers
                                        committed to diversity and inclusion.
                                        Empower your career journey with GitGirl
                                        today.
                                    </p>
                                    <div className="d-flex justify-content-center mt-3">
                                        <div>
                                            {user ? (
                                                <Link
                                                    to="/applications"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            '#B695BF',
                                                        color: 'white',
                                                        border: 'none',
                                                        borderRadius: '20px',
                                                        textAlign: 'center',
                                                        width: '100%',
                                                        marginRight: '60px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    My Applications
                                                </Link>
                                            ) : (
                                                <Link
                                                    to="/signup"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            '#B695BF',
                                                        color: 'white',
                                                        border: 'none',
                                                        borderRadius: '20px',
                                                        textAlign: 'center',
                                                        width: '100%',
                                                        marginRight: '60px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    Join Network
                                                </Link>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row justify-content-center mt-5">
                    <div className="col-md-5">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '25px',
                                    marginBottom: '40px',
                                }}
                            >
                                <div className="d-flex align-items-center">
                                    <div
                                        className="vl"
                                        style={{
                                            paddingLeft: '3px',
                                            borderLeft: '3px dotted #F2CEAE',
                                            height: '45px',
                                        }}
                                    >
                                        <h1
                                            className="mb-2"
                                            style={{
                                                marginLeft: '10px',
                                                color: '#5a414b',
                                                fontFamily: 'Trirong',
                                                fontWeight: '700',
                                            }}
                                        >
                                            For Employers
                                        </h1>
                                    </div>
                                </div>
                                <p
                                    style={{
                                        marginTop: '10px',
                                        fontFamily: 'Ariel',
                                    }}
                                >
                                    GitGirl offers employers access to a diverse
                                    pool of highly qualified women and
                                    femme-identifying individuals in tech,
                                    fostering inclusivity and enriching their
                                    talent pipelines. By leveraging GitGirl,
                                    employers can not only diversify their tech
                                    teams but also enhance their brand image by
                                    showcasing a commitment to creating an
                                    inclusive workplace, ultimately attracting
                                    top talent and fostering a positive company
                                    culture.
                                </p>
                                <div className="d-flex justify-content-center mt-3">
                                    {user ? (
                                        <div>
                                            <Link
                                                to="/createjob"
                                                type="button"
                                                className="btn btn-block"
                                                style={{
                                                    backgroundColor: 'white',
                                                    color: 'black',
                                                    border: '1px solid black',
                                                    borderRadius: '20px',
                                                    textAlign: 'center',
                                                    width: '100%',
                                                    marginRight: '60px',
                                                    marginTop: '20px',
                                                }}
                                            >
                                                Post a Job
                                            </Link>
                                        </div>
                                    ) : (
                                        <>
                                            <div>
                                                <Link
                                                    to="/signup"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            'white',
                                                        color: 'black',
                                                        border: '1px solid black',
                                                        borderRadius: '20px',
                                                        textAlign: 'center',
                                                        width: '100%',
                                                        marginRight: '60px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    Join Network
                                                </Link>
                                            </div>
                                        </>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginBottom: '150px',
                                }}
                            >
                                <img
                                    className="d-block user-select-none img-fluid"
                                    src="/src/GitGirl-team.jpg"
                                    alt="Sample"
                                    style={{
                                        borderRadius: '20px',
                                        height: '100%',
                                        width: '90%',
                                        marginLeft: '50px',
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div
                id="carouselExampleControls"
                className="carousel slide text-center carousel-dark"
                data-bs-ride="carousel"
            >
                <div className="carousel-inner">
                    {reviews.map((review, index) => (
                        <div
                            className={`carousel-item ${
                                index === activeIndex ? 'active' : ''
                            }`}
                            key={index}
                            style={{ minHeight: '400px' }}
                        >
                            <img
                                className="rounded-circle shadow-1-strong mb-4"
                                src={review.image}
                                alt={review.name}
                                style={{ width: '150px' }}
                            />
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-8">
                                    <h5 className="mb-3">{review.name}</h5>
                                    <p>{review.position}</p>
                                    <p className="text-muted">
                                        <i className="fas fa-quote-left pe-2"></i>
                                        {review.testimonial}
                                    </p>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                <button
                    className="carousel-control-prev"
                    type="button"
                    onClick={handlePrev}
                >
                    <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button
                    className="carousel-control-next"
                    type="button"
                    onClick={handleNext}
                >
                    <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    )
}

export default Home
